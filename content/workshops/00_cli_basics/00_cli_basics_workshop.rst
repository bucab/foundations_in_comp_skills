Workshop 0. Basic Linux and Command Line Usage: Workshop
================================================================

terminal_quest
--------------

.. youtube:: wkmy9fFfX5c

In this workshop your first objective is to complete the `terminal_quest`_. You
can follow the instructions on the bitbucket page linked above to get started.
Good luck.

.. _terminal_quest: https://bitbucket.org/bubioinformaticshub/terminal_quest

terminal_temple
---------------

If you finish terminal_quest quickly, you might want to try out the sequel:
`terminal_temple`_.

.. _terminal_temple: https://bitbucket.org/bubioinformaticshub/terminal_temple
