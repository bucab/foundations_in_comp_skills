import sys
print("\n\n# Task 1")


def count_nucleotides(nuclstr):
    counts = dict()
    for nucl in nuclstr:
        # Just a simple counting pattern for plain dict
        counts[nucl] = counts.get(nucl, 0) + 1
    return counts

# Just to get some test data, we'll read the first line from the next
# data file
with open("sequences.txt") as f:
    test_line = f.readline().strip()
    print(count_nucleotides(test_line))


print("\n\n# Task 2")


def process_sequence_lines(sequence_file):
    for line in sequence_file:
        line = line.strip()
        counts = count_nucleotides(line)
        # call our previous function and do some arithmetic
        print(len(line), (counts["C"] + counts["G"]) /
              (counts["A"] + counts["T"]))


with open("sequences.txt") as f:
    process_sequence_lines(f)


print("\n\n# Task 3")


def parse_annotation(line):
    data = dict()
    # use a limited split to separate out the first few constant
    # tokens from the <accession> and the rest starting with
    # <gene_symbol> <common_name>...
    _, accession, rest = line.split("|", 2)
    data['accession'] = accession
    # another limited split on a space separates out the <gene_symbol>
    # from the rest starting with <common_name> <keywordlist>
    symbol, rest = rest.split(" ", 1)
    data['gene_symbol'] = symbol
    # we can't  easily parse <common_name> because there is no delimiter
    # safe to separate it from <keywordlist> directly.
    # instead break <keywordlist> up
    kws = rest.split(";")
    # the first element of the split is <common_name> <keywordpair>,
    # which is different from the rest
    special_case = kws[0]
    kws = kws[1:]
    # split from the right side of the string, separating the keyword
    # name with the <common_name> prefixing it from the keyword value
    # to simplify parsing
    common_name_and_kw, kwval = special_case.rsplit("=", 1)
    # by definition of the right-mostspace must be the space between
    # <common_name> and the start of <keywordlist>
    common_name, kw = common_name_and_kw.rsplit(" ", 1)
    data['name'] = common_name
    # setup to accumulate keyword pairs
    data["keywords"] = []
    # special case handling of the keyword pair that were attached to
    # <common_name>
    if kw == "KW":
        data["keywords"].append(kwval)
    else:
        data[kw.lower()] = kwval
    # normal handling <keywordpair> cases
    for kw_pair in kws:
        kw, val = kw_pair.split("=")
        if kw == "KW":
            data["keywords"].append(val)
        else:
            data[kw.lower()] = val
    return data


def parse_with_metadata(sequence_file):
    # accumulate each gene's sequence + annoatation structure
    sequence_data = []
    for line in sequence_file:
        annoline = line.strip()
        # fetch the next line out of the file, advancing the iterator
        # to the next line
        seqline = sequence_file.readline().strip()
        seqdict = {
            "sequence": seqline,
            "annotations": parse_annotation(annoline)
        }
        sequence_data.append(seqdict)
    return sequence_data


with open("sequences_with_annotations.txt") as f:
    sequence_data = parse_with_metadata(f)
    print(len(sequence_data))


print("\n\n# Task 4")


def count_keywords(sequence_data):
    keywords = set()
    # create a complete set of keywords
    for record in sequence_data:
        # use set.update to add the complete iterable to the set
        keywords.update(record["annotations"]["keywords"])
    # initialize container for co-occurence table
    kwcounts = dict()
    for record in sequence_data:
        # create a set from the record's keywords for quicker lookups
        seqkws = set(record["annotations"]['keywords'])
        # count co-occurences for this keyword
        for group in keywords:
            # counter for co-occurences with the keyword group, which
            # may not yet have been created, so provide a default value
            counts_with_kw = kwcounts.get(group, {})
            for kw in keywords:
                # if both group and kw are present in this record's keywords
                # increment the count for kw
                if kw in seqkws and group in seqkws:
                    counts_with_kw[kw] = counts_with_kw.get(kw, 0) + 1
            # store the counter which which may not be stored yet
            kwcounts[group] = counts_with_kw
    return kwcounts


def print_kw_count_table(kwcounts, outfp=None):
    # if no output file handle is provided, write to STDOUT
    if outfp is None:
        outfp = sys.stdout
    kws = kwcounts.keys()
    column_widths = list(map(len, kws))
    # get the maximum length of all the keywords
    max_len_label = max(column_widths)

    # store pretty-format labels
    column_labels = []
    column_labels.append("".ljust(max_len_label))
    column_labels.extend(kws)

    # write the column labels out separated by padded pipes
    outfp.write(" | ".join(column_labels) + "\n")

    max_cell_count = -1
    max_cell_count_key = None

    # make pretty-printable table
    for row_key in kws:
        # format the row label and begin building each cell
        cells = [row_key.ljust(max_len_label)]
        row = kwcounts[row_key]
        for i, col_kw in enumerate(kws):
            width = column_widths[i]
            count = row.get(col_kw, 0)
            if count > max_cell_count:
                max_cell_count = count
                max_cell_count_key = (row_key, col_kw)
            cell_str = str(count).center(width)
            cells.append(cell_str)
        outfp.write(" | ".join(cells) + "\n")
    outfp.write("\n")
    outfp.write("Maximum Count: {} {}\n".format(
        max_cell_count, max_cell_count_key[0]))
    row = kwcounts[max_cell_count_key[0]]
    mean = sum(row.values()) / len(row)
    outfp.write("Average Count: {:0.0f}\n".format(mean))
    above_mean = []
    for key, value in row.items():
        if key == max_cell_count_key[0]:
            continue
        if value > mean * 3:
            above_mean.append(key)
    outfp.write("Above mean:\n")
    for key in above_mean:
        outfp.write("\t{}: {}\n".format(key, row[key]))


kw_table = count_keywords(sequence_data)
with open('table_file.txt', 'w') as f:
    print("Writing the counts table and report to \"table_file.txt\"")
    print_kw_count_table(kw_table, f)


print("\n\n# Task 5")

import json
'''
JSON is a great format for serializing simple data containers like the
dictionaries we created. It can represent arbitrary nesting of "objects"
and lists, as well as scalar values like numbers and strings. Python has
a module in the standard library called json which can convert to and
from Python objects to JSON strings easily.
'''

with open("saved_data.json", 'w') as f:
    json.dump(sequence_data, f)

with open('saved_data.json') as f:
    reloaded_data = json.load(f)
    print("Reloaded data matches memory:", sequence_data == reloaded_data)
