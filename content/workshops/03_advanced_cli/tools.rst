Useful tools
------------

There are many, many programs installed by default on linux systems, but only a
handful are needed in day-to-day usage.

These are some useful commands that you can explore for yourself:

- ``file <path>`` - guess what type of file (i.e. gzipped file, PDF file, etc)
  is at the location ``<path>``
- ``du`` - show the disk usage (i.e. file size) of files in a directory, or
  summarized for all files beneath a directory
- ``df`` - show the disk volumes mounted on the current filesystem and their
  capacity and usage
- ``ln -s <src> [<dest>]`` - create a symbolic link, which is a file that points
  to file ``<src>`` and is named ``<dest>`` (same as ``<src>`` filename by
  default)

The following commands are very useful but warrant some explanation and so are
described in a final video:

- ``pushd`` and ``popd`` - modify your directory stack, so you can switch
  between different directories quickly
- ``find`` - search for files with particular characteristics (e.g. filename
  pattern) recursively from a given directory
- ``xargs`` / ``fim`` - execute the same command on a list of input


.. youtube:: k_Qt2khwA7c
