curl ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_28/gencode.v28.annotation.gtf.gz --output annotation.gtf.gz

GENCODE=annotation.gtf.gz

# 1
zcat $GENCODE | grep -E -o 'gene_type "[^"]*";' | sort | uniq

# 2
zcat $GENCODE | grep 'gene_type "rRNA";' | grep -E -o 'gene_name "[^"]*";' | sort | uniq | wc -l

# 3
zcat $GENCODE | grep 'gene_type "rRNA";' | awk '$3 == "gene" {print $1, $3, $4, $5}' | wc -l

# 4
RESTURL=https://rest.ensembl.org/sequence/id
zcat $GENCODE | grep 'gene_type "rRNA";' | grep -E '\<gene\>' | grep -E -o 'ENSG[0-9\.]{13}' | xargs -P 4 -I {} curl $RESTURL/{}?type=genomic -H 'Content-type:text/x-fasta' > output.fasta
