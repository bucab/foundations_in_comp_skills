Workshop 3. Advanced CLI and Tools
==================================

This workshop covers some more advanced features of using the linux command line
that you will find very helpful when using a CLI for bioinformatics tasks.

.. toctree::
    :maxdepth: 2
    :caption: Sections

    Command line editors <editors>
    Advanced CLI <advanced_techniques>
    Useful tools <tools>
    Workshop task <03_advanced_cli_workshop>
