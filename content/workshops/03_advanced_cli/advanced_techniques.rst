Advanced command line techniques
--------------------------------

Despite being text-based, the bash shell is a very sophisticated and powerful
user interface.

.. note::

   There are many shell programs, of which bash is the most common. Different
   shells have different capabilities and syntax, and the following material is
   specific to the bash shell. If you use another shell (e.g. `tcsh`_ or `zsh`_)
   some of these techniques will likely not work.

.. _tcsh: https://en.wikipedia.org/wiki/Tcsh
.. _zsh: https://en.wikipedia.org/wiki/Z_shell

These short tutorials cover some of the more useful capabilities of the bash
shell, but are hardly exhaustive.

There are many guides and tutorials online for more comprehensive study, just a
few:

- `Bash Guide for Beginners`_
- `The Bash Academy`_ (cool, but quite incomplete as of 8/3/2017)
- `LMGTFY`_

.. _Bash Guide for Beginners: http://www.tldp.org/LDP/Bash-Beginners-Guide/html/
.. _The Bash Academy: http://guide.bash.academy/
.. _LMGTFY: http://bfy.tw/Y87

Part 1
++++++

- pipelining
- silencing ``stdout`` and ``stderr`` with ``/dev/null``

*Runtime: ~10 min*

.. youtube:: vLJOmO1WYL4

Part 2
++++++

- aliases - user-defined shortcuts for longer commands
- environment variables - text values stored in variables
- `shell expansion`_ - construct commands by string substitution and special syntax
- ``~/.bash_profile`` and ``~/.bashrc``

*Runtime: ~10 min*

.. youtube:: 7lSdSbgvAvs

.. _shell expansion: http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_03_04.html

Part 3
++++++

- the ``history`` command
- history scrollback
- history search (Ctl-r)

*Runtime: ~4 min*

.. youtube:: WG-MGFPsLhk
