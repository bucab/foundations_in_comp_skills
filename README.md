# Foundations in Computational Skills

This repo contains all the materials for the BU Bioinformatics Foundations in
Computational Skills workshops. The materials are hosted on readthedocs.org:

[BU Bioinformatics Foundations in Computational Skills](http://foundations-in-computational-skills.readthedocs.io/en/latest/)