Links to workshop and video scripts
===================================

.. toctree::
   :maxdepth: 1
   :caption: Scripts

   introduction_script.rst
   content/workshops/00_cli_basics/00_cli_basics_script
   content/workshops/02_seq_process_app/02_seq_process_app_script.rst
